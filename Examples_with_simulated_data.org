# -**- coding: utf-8; -**-
# -**- mode: org; -**-
#+STARTUP: overview, indent

#+TITLE:  Notebook - Running M2GP on simulated data
#+AUTHOR: A. Constantin
#+DATE:   2 July 2021
#+LANGUAGE: en
#+OPTIONS: *:nil num:1 toc:t
#+OPTIONS:   TeX:t LaTeX:t

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/readtheorg.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/readtheorg/js/readtheorg.js"></script>

#+PROPERTY: header-args  :session

* Python dependencies

This part loads the dependencies used in this notebook.

  #+begin_src python :results silent :session :exports code
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Used to generate samples
from sklearn.gaussian_process import GaussianProcessRegressor
# Parametric family of kernels
from sklearn.gaussian_process.kernels import (RBF, ConstantKernel as CK,
                                              WhiteKernel)

# Import model
from M2GP_model import M2GP
from MIGP_model import MIGP
  #+end_src

  #+RESULTS:

  
* Example with 1 run with irregular time-series of same length /q/

** Generate data


  Set variables
#+begin_src python :results silent :session :exports code
from simulated_data import *

# Variables to change
seed = 0    # Number of run - i.e. Seed to generate GP samples
q = 10      # Length of time-series
beta_ = 0.25 # inter-Bands correlation

# Fixed variables
n_ech = 2000 # Number of time-series - 1000 per class
tau_ = 1000  # Number of time samples within [0, 1] - Dense grid with step size of 1/tau
n_c = 2      # 2 classes
n_b = 10     # 10 bands

lambda_ = 0.01     # Variability of mean coefficients
level_ = 1.5       # RBF level
length_scale = 150 # RBF length-scale
n_level_ = 0.05    # Noise level
#+end_src


Generate train and test samples for both classes
#+begin_src python :results output :session :exports code
ct = int(n_ech/2)

# extract functions
for c in range(n_c):
    # Fixed seed for alpha for all replications
    np.random.seed(c*2)
    # Extract mean and covariances matrices
    A_c_, means_c, alpha_c = generate_mean_features(p = n_b, beta = beta_,
                                                    lambda_ = lambda_, tau = tau_)
    # Set seed - number of replication
    np.random.seed(seed)
    # Generate random multivariate samples from GPs
    Gps_, complete_GPs_, Cs_ = generate_GPs(c = c, A_c = A_c_, means_c = means_c,
                                            n_times = q, level = level_,
                                            l_c = length_scale, n_level = n_level_, 
                                            n_sig = n_ech, tau = tau_)
    # Concatenate for each class
    if c == 0:
        # Split train and test
        # Train
        Gps_train, Cs_train = Gps_[:ct, ...], Cs_[:ct]
        complete_GPs_train = complete_GPs_[:ct, ...]
        # Test
        Gps_test, Cs_test = Gps_[ct:, ...], Cs_[ct:]
        complete_GPs_test = complete_GPs_[ct:, ...]
        
        # Correlation + mean coefficients
        As_c = A_c_[np.newaxis, ...]
        alphas_c = alpha_c[np.newaxis, ...]
    else:
        # Concatenante second class to the first one

        # Split train and test
        # Train
        Gps_train = np.concatenate([Gps_train, Gps_[:ct, ...]], axis = 0)
        complete_GPs_train = np.concatenate([complete_GPs_train, complete_GPs_[:ct, ...]], axis = 0)
        Cs_train = np.concatenate([Cs_train, Cs_[:ct]], axis = 0)
        # Test
        Gps_test = np.concatenate([Gps_test, Gps_[ct:, ...]], axis = 0)
        complete_GPs_test = np.concatenate([complete_GPs_test, complete_GPs_[ct:, ...]], axis = 0)
        Cs_test = np.concatenate([Cs_test, Cs_[ct:]], axis = 0)
        
        # Correlation + mean coefficients
        As_c = np.concatenate([As_c, A_c_[np.newaxis, ...]], axis = 0)
        alphas_c = np.concatenate([alphas_c, alpha_c[np.newaxis, ...]], axis = 0)
    
# Total number of classes    
CLASSES = np.unique(Cs_train)
# Number of basis functions
J = alphas_c.shape[2]
#+end_src

#+RESULTS:

The following figure represents two samples from a GP with mean
represented in blue for class $c=1$ and band $b = 1$.

The black and red lines are processes samples, the dots are irregular
samples.

#+begin_src python :results file :session :exports both
import matplotlib.pyplot as plt
matplot_lib_filename = "figures/mean_with_samples.png"

# Time samples
t_ = np.linspace(0, tau_, tau_+1)
# Design matrix
Js = np.linspace(0, J-1, J, dtype=int)
B = np.empty((J, t_.size))
for l, t in enumerate(t_):
    B[:, l] = fourier_basis_functions(t, Js, tau_)

mu1 = np.dot(alphas_c[0, ...], B)

plt.figure(figsize=(6,4))
# band 1
b = 0
# Draw samples
plt.plot(t_ / 1000, mu1[b, :], 'b', alpha=0.5)
# Draw mean
plt.plot(t_ / 1000, complete_GPs_train[1, b+1, :], 'k', alpha=0.25)
plt.plot(Gps_train[1, 0] / 1000, Gps_train[1, b+1], 'ko')
plt.plot(t_ / 1000, complete_GPs_train[2, b+1, :], 'r', alpha=0.25)
plt.plot(Gps_train[2, 0] / 1000, Gps_train[2, b+1], 'ro')
plt.xlabel("time")
plt.tight_layout()

plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+RESULTS:
[[file:figures/mean_with_samples.png]]



** Train M2GP and MIGP models

Import the model and set parameters.

#+begin_src python :results output :exports code
# Initialize kernels
K_dep = Ck(1, constant_value_bounds=(0.1, 1e5)) * RBF(length_scale=10, length_scale_bounds=(1e-5, 1e5)) \
    + WhiteKernel(0.1)
K_indep = Ck(1, constant_value_bounds=(0.1, 1e5)) * RBF(length_scale=10, length_scale_bounds=(1e-5, 1e5)) \
    + WhiteKernel(0.1)

# Set basis function
basis_ = "fourier"
n_basis_ = int((J - 1)/2)

# Models
model = M2GP(kernel = K_dep, epsilon = 1e-8,
             base = basis_, n_basis = n_basis_,
             tau = tau_)
model_indep = MIGP(kernel = K_indep, epsilon = 1e-8,
                   base = basis_, n_basis = n_basis_,
                   tau = tau_)
# Fit
model.fit(Gps_train, Cs_train)
model_indep.fit(Gps_train, Cs_train)
#+end_src

#+RESULTS:

Now the model parameters are extracted and checked.
Estimated parameters are presented.

The following presents the predicted means for this run.
#+begin_src python :results file :session :exports both
matplot_lib_filename = "figures/estimated_means.png"
import matplotlib.pyplot as plt


fig = plt.figure(figsize=(8,12))
for c in range(2):
    means = np.dot(model.alpha_[c, ...], B)
    true_means = np.dot(alphas_c[c, ...], B)
    
    for b in range(10):
        ax = fig.add_subplot(10, 2, b*2+(c+1))
        ax.plot(t_/1000, true_means[b, :], 'k--', label = "True")
        ax.plot(t_/1000, means[b, :], 'b', label = "M2GP mean")
        ax.set_ylim(-0.5, 0.5)
        ax.set_xlabel("Class {}".format(c+1))
        if c == 0:
            ax.set_ylabel("b = {}".format(b+1))
        if b == 0:
            ax.legend()
plt.tight_layout()

plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+RESULTS:
[[file:figures/estimated_means.png]]

The black dashed line represents the true mean (used to generate
samples) and the blue continuous line is the means from M2GP.

Then the nMSE score for the means are
#+begin_src python :results output :session :exports both
def nMSE_a(x,y):
    square_error = np.square(y - x)
    std_true = np.square(x - np.mean(x))
    return square_error.mean() / std_true.mean()

print("c = 0, nMSE:", nMSE_a(np.dot(model.alpha_[0, ...], B).reshape(-1), np.dot(alphas_c[0, ...], B).reshape(-1)))
print("c = 1, nMSE:", nMSE_a(np.dot(model.alpha_[1, ...], B).reshape(-1), np.dot(alphas_c[1, ...], B).reshape(-1)))
#+end_src

#+RESULTS:
: c = 0, nMSE: 0.4964621175547408
: c = 1, nMSE: 0.4438979325402364

Then the cos score for $AA^\top$ are
#+begin_src python :results output :session :exports both
def cos_mat(A, B):
    BA = np.trace(np.dot(B.T, A))
    AA = np.trace(np.dot(A.T, A))
    BB = np.trace(np.dot(B.T, B))
    return 1 - BA / np.sqrt(AA * BB)

for c in range(2):
    AA_tmp = np.dot(As_c[c, ...], As_c[c, ...].T)
    print("Cosine scores, c = {}: {:.6f}".format(c, cos_mat(AA_tmp, model.AA_T_[c, ...])))
#+end_src

#+RESULTS:
: Cosine scores, c = 0: 0.000377
: Cosine scores, c = 1: 0.000377

Finally the absolute difference of length scale are
#+begin_src python :results output :session :exports both
print("Length scale of the optimized kernel (c=1): {:.3f}\n".format(np.exp(model.kernels["0"].theta)[1]))

# Score of length-scale
print("Absolute differences: {:.3f} (c=1), {:.3f} (c=2)".format(
    np.abs(150 - np.exp(model.kernels["0"].theta)[1]),
    np.abs(150 - np.exp(model.kernels["1"].theta)[1])))
#+end_src

#+RESULTS:
: Length scale of the optimized kernel (c=1): 149.661
: 
: Absolute differences: 0.339 (c=1), 0.339 (c=2)

** Compute classification accuracy and imputation

Prediction on the testing set
#+begin_src python :results output :session :exports code
results = model.predict(Gps_test)
results_indep = model_indep.predict(Gps_test)
#+end_src

#+RESULTS:

Compute Overall Accuracy score
#+begin_src python :results output :session :exports both
from sklearn.metrics import accuracy_score

print("OA MIGP:", accuracy_score(results_indep, Cs_test))
print("OA M2GP:", accuracy_score(results, Cs_test))
#+end_src

#+RESULTS:
: OA MIGP: 0.702
: OA M2GP: 0.7365

Imputation of missing values, the imputation is done on 100
timestamps, $\{t*l / 100\}_{l = 0}^99$
#+begin_src python :results output :session :exports both
# nMSE score
def nMSE_r(y_true, y_pred):
    y_true_mean = np.mean(y_true)
    std_true = np.square(y_true - y_true_mean)
    return np.square(y_pred - y_true).mean() / std_true.mean()

# Times to reconstruct
t_input = np.arange(0, 1000, 10)
t_input = t_input.reshape(-1, 1)

# Reconstruct each time-series within test set
r_ = []
r_indep = []
for i in range(Gps_test.shape[0]):
    r_.append(model.input_missing_values(Gps_test[i, :], t_input, c = None))
    r_indep.append(model_indep.input_missing_values(Gps_test[i, :], t_input, c = None))

r_ = np.array(r_)
r_indep = np.array(r_indep)

# Print scores
print("nMSE MIGP:", nMSE_r(complete_GPs_test[:, 1:, t_input].flatten(), r_indep[:, 1:, :].flatten()))
print("nMSE M2GP:", nMSE_r(complete_GPs_test[:, 1:, t_input].flatten(), r_[:, 1:, :].flatten()))
#+end_src

#+RESULTS:
: nMSE MIGP: 0.15032380870324127
: nMSE M2GP: 0.15029009360201026


Finally, the following prints one visual reconstruction for the 10 bands
#+begin_src python :results file :session :exports both
import matplotlib.pyplot as plt
matplot_lib_filename = "figures/imputation_example.png"

i = 10

fig = plt.figure(figsize=(8,14))

    
for b in range(10):
    ax = fig.add_subplot(10, 1, (b+1))
    ax.plot(t_/1000, mu1[b, :], 'b', label = "GP mean")
    ax.plot(t_input/1000, r_[i, b+1], 'r', label = "M2GP imputation")
    ax.plot(t_input/1000, complete_GPs_test[i, b+1, t_input], 'k', label = "True sample")
    ax.plot(Gps_test[i, 0]/1000, Gps_test[i, b+1], 'ko', label = "Observed value")
    ax.set_ylim(-4.2, 4.2)
    ax.set_ylabel("b = {}".format(b+1))
    if b == 0:
        ax.legend()
plt.tight_layout()

plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+RESULTS:
[[file:figures/imputation_example.png]]
 The GP mean (nearly 0) is represented by a blue line, the sample from
 the GP is represented in a black line and the associated sample which
 is used in the model is represented by the black dots.
 The imputation is represented in red for this sample.
