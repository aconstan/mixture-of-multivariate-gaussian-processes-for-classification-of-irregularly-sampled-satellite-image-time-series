# coding: utf-8
import imp
import warnings
from operator import itemgetter
import numpy as np

from scipy.linalg import cholesky, cho_factor, cho_solve, solve_triangular, solve, lstsq
from scipy.linalg.lapack import dpotrf, dpotri
from scipy.linalg import LinAlgWarning, LinAlgError
from scipy.optimize import fmin_l_bfgs_b

from sklearn.externals.joblib import Parallel, delayed

from sklearn.base import BaseEstimator, clone
from sklearn.utils import check_random_state
from sklearn.utils.validation import check_array

from sklearn import preprocessing

# Kernel depedency
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, WhiteKernel
from sklearn.exceptions import ConvergenceWarning
# Score metrics
from sklearn.metrics import accuracy_score




def constrained_optimization(obj_func, initial_theta, bounds,
                             X, t, c, optimizer):
    if optimizer == "fmin_l_bfgs_b":
        theta_opt, func_min, convergence_dict = \
                            fmin_l_bfgs_b(obj_func, initial_theta,
                                          args=(True, X, t, c),
                                          bounds=bounds,
                                          maxls=20)
        if convergence_dict["warnflag"] != 0:
            warnings.warn("fmin_l_bfgs_b terminated abnormally with the "
                          " state: %s" % convergence_dict,
                          ConvergenceWarning)
    elif callable(optimizer):
        theta_opt, func_min = optimizer(obj_func, initial_theta,
                                        args=(True, X, t, c),
                                        bounds=bounds)
    else:
        raise ValueError("Unknown optimizer %s." % optimizer)

    return theta_opt, func_min

def fourier_basis_functions(t, J, tau):
    """
    Input : t,   float, instant of observation
            J,     int, number of basis function
            tau, float, observed period
    Output: array[float], fourier basis function
    """
    n_bases = J.size
    n_functions = int((J.size-1)/2)
    b = np.empty((n_bases,))
    b[0] = 1
    for j in range(1, 1+n_functions):
        b[j] = np.cos(2*np.pi*j*t/tau)
        b[n_functions+j] = np.sin(2*np.pi*j*t/tau)

    return b


class MIGP(BaseEstimator):
    def __init__(self, kernel=None, epsilon=1e-12,
                 means=None,
                 base="fourier", n_basis=10, tau=None,
                 optimizer="fmin_l_bfgs_b",
                 n_restarts_optimizer=0,
                 random_state=None):
        # Time range (0, tau) for the time-series
        self.tau = tau

        # Kernel definition - sklearn function
        self.kernel = kernel
        self.epsilon = epsilon

        # Mean, design matrix definition
        self.means_ = means
        if means != None:
            print("Model initialized with a mean.")
            print(" The dictionnary mean must be evaluable")
            print(" for any inputed discretized time-series.")
        else:
            self.alpha_ = None
            self.B = None
            self.dict_position = {}

            if callable(base):
                self.n_basis_ = n_basis
                self.func_base = base
            elif base == "polynomial":
                self.n_basis_ = n_basis
                self.func_base = lambda t_, j, tau_: np.power(t_ / tau_, j)
            elif base == "fourier":
                self.n_basis_ = n_basis * 2 + 1
                self.func_base = fourier_basis_functions
            # else:
            #     self.n_basis_ = n_basis_+2
            #     self.func_base = exp_basis_functions

        # Linear dependencies GP
        self.AA_T_ = None
        self.inv_AA_T_ = None

        # Optimization parameters
        self.optimizer = optimizer
        self.n_restarts_optimizer = n_restarts_optimizer
        self.log_marginal_likelihood_value_ = 0

        # Data settings
        self.random_state = random_state


    def fit(self, X, y, compute_likelihood=False):
        """
        Input : X, array-like, shape = (n_samples,
                                        1 + n_features, list of T_y instants)
                   the first feature is the observation instants of X
                y, array: class labels (transformed using preprocessing);
                   structured as [n samples]
        Output: self
        """
        # Init random state
        self._rng = check_random_state(self.random_state)

        # Prepare data: split sample time & features
        X_train_ = np.copy(X[:, 1:])
        self.n = X_train_.shape[0]
        self.n_bands_ = X_train_.shape[1]
        t_ = np.copy(X[:, 0])

        # Init design matrix
        self.init_design_matrix(t_)

        # Preprocess classes
        le = preprocessing.LabelEncoder()
        y_train_ = le.fit_transform(y)
        self.classes_ = le.classes_
        self.n_classes_ = le.classes_.size

        # Compute weights
        self.index_C = [np.where(y_train_ == c)[0]
                        for c in range(self.n_classes_)]

        self.weights_ = np.array([float(ind_c.size)/self.n
                                  for ind_c in self.index_C])

        # Initialization alpha
        if self.means_ == None:
            self.alpha_ = np.empty([self.n_classes_, self.n_bands_, self.n_basis_])
        # Initialization of A
        self.AA_T_ = np.repeat(np.eye(self.n_bands_)[np.newaxis, :, :],
                            self.n_classes_, axis = 0)
        self.inv_AA_T_ = self.AA_T_.copy()

        # Kernels definition
        kernel_ = C(1.0, constant_value_bounds=(1e-5, 1e5)) \
            * RBF(1.0, length_scale_bounds=(1e-05, 1e5)) \
            + WhiteKernel(noise_level=0.1,
                          noise_level_bounds=(1e-10, 1e+1)) \
            if self.kernel is None else self.kernel

        self.kernels = {"{0}".format(c): clone(kernel_)
                        for c in range(self.n_classes_)}

        if self.optimizer:
            # Objectif function for fixed c
            def obj_func(theta, eval_gradient=True, *args):
                if eval_gradient:
                    lml, grad = self.log_likelihood(theta,
                                                    eval_gradient,
                                                    X_train_C = args[0],
                                                    t_C = args[1], c = args[2],
                                                    compute_optimal_parameters = True)
                    return -lml, -grad
                else:
                    return -self.log_likelihood(theta, eval_gradient,
                                                X_train_C = args[0],
                                                t_C = args[1], c = args[2],
                                                compute_optimal_parameters = True)

            # Optimization on c and b at once - Not working like this
            thetaC_ = Parallel(n_jobs=-1)(
                delayed(constrained_optimization)(
                    obj_func,
                    self.kernels["{0}".format(c)].theta,
                    self.kernels["{0}".format(c)].bounds,
                    X_train_[self.index_C[c], :],
                    t_[self.index_C[c]],
                    c, self.optimizer)
                for c in range(self.n_classes_))

            thetaC = {"theta{0}".format(c): [thetaC_[c]]
                      for c in range(self.n_classes_)}

            # Serial implementation

            # thetaC = {"theta{0}".format(c):
            #           [(constrained_optimization(
            #               obj_func,
            #               self.kernels["{0}".format(c)].theta,
            #               self.kernels["{0}".format(c)].bounds,
            #               X_train_[self.index_C[c], :],
            #               t_[self.index_C[c]],
            #               c, self.optimizer))]
            #           for c in range(self.n_classes_)}

            if (self.n_restarts_optimizer > 0):
                # Additional runs are performed from log-uniform chosen initial
                # theta bounds
                kernel_bounds = [self.kernels["{0}".format(c)].bounds
                                 for c in range(self.n_classes_)]

                if not np.isfinite(kernel_bounds).all():
                    raise ValueError("Multiple optimizer restarts"
                                     "(n_restarts_optimizer>0)"
                                     "requires that all bounds are finite.")

                for iteration in range(self.n_restarts_optimizer):
                    for c in range(self.n_classes_):
                        theta_initial = \
                                self._rng.uniform(
                                self.kernels["{0}".format(c)].bounds[:, 0],
                                self.kernels["{0}".format(c)].bounds[:, 1])

                        thetaC["theta{0}".format(c)].append(
                            constrained_optimization(
                                obj_func, theta_initial,
                                self.kernels["{0}".format(c)].bounds,
                                X_train_[self.index_C[c]],
                                t_[self.index_C[c]],
                                c, self.optimizer))
            
            # Select result from run with minimal (negative) log-marginal
            # likelihood
            self.log_likelihood_value_ = 0
            for c in range(self.n_classes_):
                lml_values_cb = list(map(itemgetter(1),
                                         thetaC["theta{0}".format(c)]))
                i_min = np.argmin(lml_values_cb)
                # Store theta
                self.kernels["{0}".format(c)].theta = \
                        thetaC["theta{0}".format(c)][i_min][0]
                # Store lml
                self.log_likelihood_value_ += \
                        thetaC["theta{0}".format(c)][i_min][1]

        # Compute alpha and AA_T for fixed or optimized theta
        for c in range(self.n_classes_):

            T_un_c, ind_c = np.unique(np.hstack(t_[self.index_C[c]]), return_inverse = True)
            B_c = self.design_matrix(T_un_c, in_memory = True)
            K_ = self.kernels["{0}".format(c)](T_un_c.reshape(-1, 1))
            K_[np.diag_indices_from(K_)] += self.epsilon

            if self.means_ == None:
                # Compute hat(alpha)
                left_part = np.zeros((self.n_basis_, self.n_basis_))
                right_part = np.zeros((self.n_basis_, self.n_bands_))
                
                # Compute summation terms for alpha
                start = 0
                for ind_ in self.index_C[c]:
                    Ti = t_[ind_].shape[0]
                    indexes = ind_c[start:(start + Ti)]
                    start += Ti
                    # Compute design matrix
                    Bi = B_c[:, indexes]
                    
                    # Compute kernel matrix
                    K = K_[np.ix_(indexes, indexes)]

                    # Solve linear problem
                    L = cho_factor(K, lower=True)
                    M_left = cho_solve(L, Bi.T)
                    M_right = cho_solve(L, np.vstack(X_train_[ind_]).T)
                    
                    # Update left and right parts
                    left_part += np.dot(Bi, M_left)
                    right_part += np.dot(Bi, M_right)

                # Get optimal alpha for class c and band b
                # warning from scipy.linalg is not printed
                warnings.filterwarnings("error", category = LinAlgWarning)
                for b in range(self.n_bands_):
                    try:
                        self.alpha_[c, ...] = np.transpose(solve(left_part, right_part))
                    except (LinAlgWarning, LinAlgError):
                        #print('Ill-conditioned matrix, alpha solved using least_square')
                        self.alpha_[c, ...] = np.transpose(lstsq(left_part, right_part)[0])
                warnings.resetwarnings()
       
        # Compute log_likelihood_
        if compute_likelihood:
            self.log_likelihood_value_ = 0
            for c in range(self.n_classes_):
                self.log_likelihood_value_ += \
                        self.log_likelihood(None, None,
                                            X_train_[self.index_C[c], :],
                                            t_[self.index_C[c]],
                                            c, False)
        return self

    def predict(self, X):
        """Return classes estimates for the test vector X, using MAP.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        Returns
        -------
        C : array-like, shape = (n_samples)
            Returns the class for each sample in the model.
        """
        return self.classes_[np.argmax(self.predict_proba(X), axis=-1)]

    def predict_proba(self, X):
        """Return probability estimates for the test vector X.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        Returns
        -------
        C : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute ``classes_``.
        """
        X_ = X[:, 1:]
        t = X[:, 0]
        n_samples = X_.shape[0]
        probas = np.zeros([n_samples, self.n_classes_])    # save probabilities

        # Precompute kernel and design matrix
        T_un, ind_ = np.unique(np.hstack(t), return_inverse = True)
        B_ = self.design_matrix(T_un, in_memory = False)
        K_ = [self.kernels["{0}".format(c)](T_un.reshape(-1, 1)) \
              for c in range(self.n_classes_)]

        # Precompute log_det AA_T
        log_det_AA_T_ = []
        for c in range(self.n_classes_):
            AA_T = self.AA_T_[c]
            AA_T[np.diag_indices_from(AA_T)] += self.epsilon
            low = cho_factor(AA_T, lower=True)
            log_det_AA_T_.append(np.log(np.diag(low[0])).sum())
            K_[c][np.diag_indices_from(K_[c])] += self.epsilon

        starts = 0
        for i in range(n_samples):
            # Len of time-series
            Ti = t[i].shape[0]
            indexes = ind_[starts:(starts + Ti)]
            # Update starts
            starts += Ti

            # Loop on each class
            for c in range(self.n_classes_):
                log_like = 0
                # Compute design matrix (t_ny dependency)
                Bi = B_[:, indexes]
                # Center X
                Xc = np.vstack(X_[i]) - np.dot(self.alpha_[c], Bi)

                # Compute kernel matrix
                K = K_[c][np.ix_(indexes, indexes)]
                L = cho_factor(K, lower=True)

                # Compute numerator
                Q = cho_solve(L, Xc.T) # Sigma^-1 * Xc.T
                q = np.dot(Q, np.dot(
                    self.inv_AA_T_[c], Xc))  # Q * inv_AA_T_ * Xc

                # log( exp(-1/2 q) ) -p/2 logdet Ki -Ti/2 logdet AA_T - cste
                log_like = - 0.5 * np.trace(q) \
                           - self.n_bands_ * np.log(np.diag(L[0])).sum() \
                           - Ti * log_det_AA_T_[c] \
                           - 0.5 * Ti * self.n_bands_ * np.log(2*np.pi)
                

                # log_likelihood + log_prior #{Zi = c} * P(y | Z = c)
                probas[i, c] = log_like + np.log(self.weights_[c])

            # scale for exp
            prior_max = probas[i, :].max()
            # LogSumExp trick for denominator
            LSE = prior_max + np.log(np.sum(np.exp(probas[i] - prior_max)))
            # compute posterior: #{Zi = c} * P(y | Z = c) / sum_i(P(y | Z = i))
            probas[i] = np.exp(probas[i] - LSE)

        return probas

    def score(self, X, y_true):
        """Return probability estimates for the test vector X.
        Parameters
        ----------
        X:      array-like, shape = (n_samples, 1 + n_features, T_y instants)
                the first feature is the observation instants of X
        y_true: array-like, shape = (n_samples, 1 class)
        Returns
        -------
        Score : float,
            Returns the model's accuracy.
        """
        return accuracy_score(y_true, self.predict(X))

    def input_missing_values(self, X, t, c = None, return_std = False, return_cov = False):
        """Return time-series values estimated for the test vector X, using MAP.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        t: array_like, shape = (n(y) instants)
            contains the n(y) instants to input to signal X
        c:  int, default None
            class of the signal, if c = None: Applied Bayes decision rule
        return_std: bool, default False
            if True returns standard deviation of the signal to the mean.
        return_cov: bool, default False
            if True returns the covariance matrix associated to the process
        Returns
        -------
        mean_values: array-like, shape = (n(y), 1 + n_features)
            Returns the n(y) instants values for each feature.
        var: array-like float, shape = (n(y), n_features)
            if return_std, Variance evaluated on each point of the inputed time-series
        cov: Array-like, shape = (n_features, n(y), n(y))
            Covariance matrix associated to the inputed elements
        """
        if return_std and return_cov:
            raise RuntimeError("Not returning standard deviation of predictions when "
                               "returning full covariance.")
        t_ = check_array(t)
        
        x_ = np.array([X[b] for b in range(1, X.shape[0])])
        t_obs = X[0].reshape([-1,1])
        

        if not hasattr(self, "kernels"): # Unfitted;no prediction
            raise RuntimeError("Model not fitted.")
        else:
            # Create predictive mean/var values and covariance matrix,
            #    mean values is containing times and values for each band
            mean_values = np.zeros([self.n_bands_ + 1, t_.shape[0]])
            if return_cov:
                cov_mat = np.zeros([self.n_bands_, t_.shape[0], t_.shape[0]])
            elif return_std:
                var_values = np.zeros([self.n_bands_, t_.shape[0]])


            # Compute mean (and var, cov)
            if (c == None):
                # Prediction when class is unknown
                probas_c = self.predict_proba(np.array([X]))
                probas_c[0, probas_c[0,:] < 1e-20] = 0    # avoid approximation on zero machine

                if return_cov:
                    # Return mean and covariance matrix
                    expectations_squared_c = np.zeros([self.n_bands_, t_.shape[0], t_.shape[0]])

                    for ci in range(self.n_classes_):
                        # Get values when c is known
                        expectations_cov_c = self.input_missing_values(X, t_,
                                                                       c = self.classes_[ci],
                                                                       return_cov=True)
                        # Save and ponderate them
                        mean_values += expectations_cov_c[0] * probas_c[0,ci]
                        # save covariance matrix for each class, weighted
                        cov_mat += expectations_cov_c[1] * probas_c[0,ci]

                        # compute correction term
                        for b in range(self.n_bands_):
                            expectations_squared_c[b]+= \
                                                np.dot(expectations_cov_c[0][(1+b):(2+b),:],
                                                       expectations_cov_c[0][(1+b):(2+b),:].T) \
                                                       * probas_c[0,ci]
                    
                    # correction terms in covariance
                    cov_mat += expectations_squared_c - \
                            np.matmul(mean_values[1:,:].reshape([self.n_bands_,t_.shape[0],1]),
                                      mean_values[1:,:].reshape([self.n_bands_,1,t_.shape[0]]))
                    
                    cov_mat_negative = cov_mat < 0
                    if np.any(cov_mat_negative):
                        warnings.warn("Corrected covariance matrix smaller than 0. "
                                      "Setting those values to 0.")
                        cov_mat[cov_mat_negative] = 0.0
                    
                elif return_std:
                    # Save expectations and std deviations for each classe, weighted by
                    #   its probability to belong to class c
                    expectations_squared_c = np.zeros([self.n_bands_, t_.shape[0]])
                    
                    for ci in range(self.n_classes_):
                        # Get values when c is known
                        expectations_std_c = self.input_missing_values(X, t_,
                                                                       c = self.classes_[ci],
                                                                       return_std=True)
                        # Save and ponderate them
                        mean_values += expectations_std_c[0] * probas_c[0,ci]
                        expectations_squared_c += expectations_std_c[0][1:,:] \
                                                  * expectations_std_c[0][1:,:] * probas_c[0,ci]
                        # we use square since square root is the output
                        var_values += expectations_std_c[1]*expectations_std_c[1]*probas_c[0,ci]

                    # correction terms in variance
                    var_values += expectations_squared_c - mean_values[1:,:]*mean_values[1:,:]
                    
                    x_var_negative = var_values < 0
                    if np.any(x_var_negative):
                        warnings.warn("Corrected variances smaller than 0. "
                                      "Setting those variances to 0.")
                        var_values[x_var_negative] = 0.0

                # Case when return_cov and return_std are False
                else:
                    expectations_c = np.array([self.input_missing_values(X, t_,
                                                                         c = self.classes_[ci]) \
                                               *probas_c[0,ci]
                                               for ci in range(self.n_classes_)])
                    mean_values = expectations_c.sum(axis = 0)

            else:
                # Prediction when class is known
                mean_values[0, :] = t_[:,0]               # Save temporal instants
                c_ = np.where(self.classes_ == c)[0][0]      # Uses labelEncoder defined in fit

                # Extract kernel
                K = clone(self.kernels["{0}".format(c_)])
                Sigma = K(t_obs.reshape([-1, 1]))
                L = cho_factor(Sigma, lower=True)

                # Compute mean - multi-dimentional centered output, y in formula
                if self.means_ == None:
                    # Compute design matrix
                    Bi = self.design_matrix(t_obs)
                    mean = np.dot(self.alpha_[c_], Bi)
                else:
                    # Extract mean
                    mean = self.means_["{0}".format(c)][t_C[t_obs]]
                    
                # Center X_train_
                Xc = x_ - mean

                # Compute mean (right part)
                K_t_obs = K(t_, t_obs)
                K_inv_prod_K_trans = cho_solve(L, K_t_obs.T)
                x_mean = Xc.dot(K_inv_prod_K_trans)

                # Compute mean on target points
                if self.means_ == None:
                    # Compute design matrix
                    B = self.design_matrix(t_)
                    mean = np.dot(self.alpha_[c_], B)
                else:
                    # Extract mean
                    mean = self.means_["{0}".format(c)][t_C[t_]]
                mean_values[1:, :] = mean + x_mean # undo normal
                
                if return_cov:
                    # Compute K(t*) - K_trans.T (K^-1) K_trans
                    v = cho_solve(L, K_t_obs.T) # Line 5
                    cov_mat = np.kron(K(t_) - K_t_obs.dot(v),
                                      self.AA_T_[c_]) # Line 6

                elif return_std:
                    # compute inverse K_inv of K based on its Cholesky
                    # decomposition L and its inverse L_inv
                    L_inv = solve_triangular(L[0].T,
                                             np.eye(L[0].shape[0]))
                    K_inv = L_inv.dot(L_inv.T)

                    # Compute variance of predictive distribution
                    var_values_temporal = K.diag(t_)
                    var_values_temporal -= np.einsum("ij,ij->i",
                                                     np.dot(K_t_obs, K_inv), K_t_obs)
                    Sigma_kron_AA_T = np.kron(var_values_temporal, self.AA_T_[c_])
                    for b in range(self.n_bands_):
                        var_values[b] = Sigma_kron_AA_T[b, b::self.n_bands_]

                    # Check if any of the variances is negative because of
                    # numerical issues. If yes: set the variance to 0.
                    x_var_negative = var_values[:] < 0
                    if np.any(x_var_negative):
                        warnings.warn("Predicted variances smaller than 0. "
                                      "Setting those variances to 0.")
                        var_values[x_var_negative] = 0.0
                        
                        
        if return_cov:
            return mean_values, cov_mat
        if return_std:
            return mean_values, np.sqrt(var_values)
        return mean_values


    
    def log_likelihood(self, theta=None, eval_gradient=False,
                       X_train_C=None, t_C=None, c=None,
                       compute_optimal_parameters = False):
        """Compute the LL, and the Gradient for a given class and band
        Parameters
        ----------
        theta      , list of theta (c and b)
        eval_gradient, evaluation of the gradient
        args    [0], X_train_C, array training samples for class c
                [1], t_C      , array of lists, observation instants
                [2], c, class considered
        Returns
        -------
        LML, grad LML
        """
        nc = X_train_C.shape[0]

        # Check params
        if (theta is None) and (eval_gradient is True):
            raise ValueError(
                "Gradient can only be evaluated for theta!=None")
            return self.log_marginal_likelihood_value_

        # Initialization
        if eval_gradient:
            log_likelihood_gradient_ = np.zeros([theta.size])
        log_likelihood_ = 0

        # Set Kernels
        if theta is None:
            kernel_ = self.kernels["{0}".format(c)]
        else:
            kernel_ = self.kernels["{0}".format(c)].clone_with_theta(theta)

        # Precompute kernel and design matrix
        T_un, ind_ = np.unique(np.hstack(t_C), return_inverse = True)
        B_ = self.design_matrix(T_un, in_memory = True)
        if eval_gradient:
            K_, K_gradient = kernel_(T_un.reshape(-1, 1), eval_gradient=True)
        else:
            K_ = kernel_(T_un.reshape(-1, 1))
        K_[np.diag_indices_from(K_)] += self.epsilon

        
        sum_log_det_ = 0
        N_ = np.zeros((self.n_bands_, self.n_bands_))
        # # First step: Compute G, D
        if not compute_optimal_parameters and self.means_ == None:
            alpha = self.alpha_[c, ...]
        elif self.means_ == None:
            # Compute optimal alpha and compute N_
            G = np.zeros((self.n_basis_, self.n_basis_))
            D = np.zeros((self.n_basis_, self.n_bands_))

            starts = 0
            for i in range(nc):
                # Len of time-series
                Ti = t_C[i].shape[0]
                indexes = ind_[starts:(starts + Ti)]
                # Compute design matrix
                Bi = B_[:, indexes]

                # Compute kernel matrix
                K = K_[np.ix_(indexes, indexes)]

                # Solve linear problem
                L = cho_factor(K, lower=True)
                M_G = cho_solve(L, Bi.T)
                M_D = cho_solve(L, np.vstack(X_train_C[i]).T)

                # Update leaft and right parts
                G += np.dot(Bi, M_G)
                D += np.dot(Bi, M_D)

                # Update starts
                starts += Ti
            
            # Avoid to print warning from scipy.linalg
            warnings.filterwarnings("error", category = LinAlgWarning)
            try:
                alpha = np.transpose(solve(G, D))
            except (LinAlgWarning, LinAlgError):
                alpha = np.transpose(lstsq(G, D)[0])
            warnings.resetwarnings()
            
        # Second Step: compute N and its inverse - Requires for likelihood
        AA_T_ = self.AA_T_[c, ...]
        # Compute Log-likelihood terms
        starts = 0
        for i in range(nc):
            # Len of time-series
            Ti = t_C[i].shape[0]
            indexes = ind_[starts:(starts + Ti)]
            starts += Ti
            # Compute mean
            if self.means_ == None:
                # Compute design matrix
                Bi = B_[:, indexes]
                mean = np.dot(alpha, Bi)
            else:
                # Extract mean
                mean = self.means_["{0}".format(c)][t_C[i]]
                    
            # Compute kernel matrix
            K = K_[np.ix_(indexes, indexes)]
            
            # Solve linear problem
            L = cho_factor(K, lower=True)
            # Update \sum log det Sigma_i
            sum_log_det_ += 2*np.log(np.diag(L[0])).sum()
            # Compute N
            Xc = np.vstack(X_train_C[i]) - mean
            Sigma_inv_dot_xc_T = cho_solve(L, Xc.T)
            
            N_ += np.dot(Xc, Sigma_inv_dot_xc_T)
            
        sum_Ti_ = starts
        # Invert AA_T_ inverse (I_p)
        inv_AA_T_ = AA_T_
            
        if eval_gradient:  # This part could be computed faster wt trace
            # Compute gradient
            starts = 0
            for i in range(nc):
                # Len of time-series
                Ti = t_C[i].shape[0]
                indexes = ind_[starts:(starts + Ti)]
                starts += Ti  
                # Compute mean
                if self.means_ == None:
                    # Compute design matrix
                    Bi = B_[:, indexes]
                    mean = np.dot(alpha, Bi)
                else:
                    # Extract mean
                    mean = self.means_["{0}".format(c)][t_C[i]]
                # Compute kernel matrix
                Sigma = K_[np.ix_(indexes, indexes)]
                Sigma_gradient = K_gradient[np.ix_(indexes, indexes)]

                # Solve linear problem
                L = cho_factor(Sigma, lower=True)
                Sigma_inv = cho_solve(L, np.eye(L[0].shape[0]))

                Xc = (np.vstack(X_train_C[i]) - mean)
                if Xc.ndim == 1:
                    Xc = Xc[..., np.newaxis]
                beta_T = cho_solve(L, Xc.T)

                grad_ = self.n_bands_ * Sigma_inv \
                        - np.dot(beta_T, np.dot(inv_AA_T_, beta_T.T))

                # Update gradient
                for theta_ in range(theta.size):
                    log_likelihood_gradient_[theta_] -= np.trace(
                        np.dot(grad_, Sigma_gradient[..., theta_]))

        # Third step: Compute likelihood
        log_likelihood_ -= self.n_bands_*sum_log_det_ \
                           + np.trace(np.dot(N_, inv_AA_T_))
        
        # Return LL
        if eval_gradient:
            return 0.5*log_likelihood_, 0.5*log_likelihood_gradient_
        else:
            return 0.5*log_likelihood_


    def init_design_matrix(self, t_):
        """ Initialization of the design matrix
        Compute only once the whole matrix then for each call copy only
        the corresponding part.
        """
        # Compute position of each temporal
        T = np.unique(np.hstack(t_))
        if self.tau is None:
            self.tau = int(2 * T.max()) + 1      # Save maximum to define tau
        self.dict_position = {ti: i for i, ti in enumerate(T)}

        # Precompute the whole basis
        J = np.linspace(0, self.n_basis_-1,
                        self.n_basis_, dtype=int)
        self.B = np.empty((self.n_basis_, T.size))

        for i, t in enumerate(T):
            # func_base should return all the basis for a given t
            self.B[:, i] = self.func_base(t, J, self.tau)

    def design_matrix(self, t, in_memory=False):
        """
        Matrix B in the paper.
        Conditions on band must be written here to design the model.

        Input : t, observation instants
        Output: B(t)
        """
        if in_memory is True:
            positions = [self.dict_position[t_] for t_ in t]
            B = self.B[:, positions]
        else:
            len_t = len(t)

            B = np.zeros((self.n_basis_, len_t))
            j = np.linspace(0, self.n_basis_-1, self.n_basis_, dtype=int)
            for l, t_ in enumerate(t):
                B[:, l] = self.func_base(t_, j, self.tau)

        return B
