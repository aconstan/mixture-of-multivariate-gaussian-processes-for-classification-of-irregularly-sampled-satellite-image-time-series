# coding: utf-8

import numpy as np


# Scikit kernels
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as Ck, WhiteKernel


def fourier_basis_functions(t, J, tau):
    """ Fourier basis functions
    Write Fourier base to generate means

    Input : t,   float, instant of observation
            J,     int, number of basis function
            tau, float, observed period
    Output: array[float], fourier basis function
    """
    n_bases = J.size
    n_functions = int((J.size-1)/2)
    b = np.empty((n_bases,))
    # Constant function
    b[0] = 1
    # cos / sin functions
    for j in range(1, 1+n_functions):
        b[j] = np.cos(2*np.pi*j*t/tau)
        b[n_functions+j] = np.sin(2*np.pi*j*t/tau)

    return b

def generate_mean_features(p = 3, beta = 0, lambda_ = 0.1, tau = 1):
    """ Generate mean and features (matrix to combine linearly GP samples)

    Input : p,         int, number of dimensions (bands)
            beta,    float, between 0 and 1, beta in the paper - correlation strength
            lambda_, float, variability of mean coefficients
            tau,       int, observed period
    Output: (array[p, p], array[p, tau], array[p, J]), floats,
            linear correlation between GPs, means on dense grid, mean coefficients
    """
    # Timestamps
    t_ = np.arange(0, tau+1)
    # Fourier basis functions
    J = 5*2+1   # Number of bases
    Js = np.linspace(0, J-1, J, dtype=int)
    B = np.empty((J, t_.size))
    for l, t in enumerate(t_):
        B[:, l] = fourier_basis_functions(t, Js, tau)
    alpha_c = np.empty((p, J))

    # Write mean coefficients
    alpha_c[:, 0] = np.random.normal(0, lambda_, size = p)
    for b in range(p):
        alpha_c[b, 1:] = np.random.normal(0, lambda_*2, size = J-1)

    # Generate means
    means = np.dot(alpha_c, B)

    # Bands - create AAt matrix
    AA_T_c = np.full((p, p), fill_value = beta)
    AA_T_c[np.diag_indices_from(AA_T_c)] = 1
    # Compute A_c from AA_T, beta < 1
    A_c = np.linalg.cholesky(AA_T_c)
        
    return A_c, means, alpha_c

def generate_GPs(c, A_c, means_c, n_times = 10, level = 1, l_c = 150, n_level = 0.05, n_sig = 100, tau = 1000):
    """ Generate Gaussian processes

    Input : c,            int, selected class
            A_c, array[float], Linear combination of bands
            n_times,      int, number of observations (q)
            level,      float, RBF Level
            l_c,        float, RBF Length scale
            n_level,    float, Noise Level
            n_sig,        int, number samples
            tau,          int, observed period
    Output: (array[n_sig, p + 1, ], array[n_sig, p+1, tau], array[n_sig, ]), objects,
            Irregularly sampled GPs, GPs on dense grid, Associated class
    """
    # Initialization of GPs and class
    complete_GPs = np.empty((n_sig, A_c.shape[0] + 1, tau+1))
    GPs = [[] for _ in range(n_sig)]
    Cs = []
    # Number of times
    all_times = np.arange(tau+1)
    null_mean = np.zeros_like(all_times)

    # Draw random variate Gaussian
    for b in range(A_c.shape[0]):
        # Generate kernel
        Kbc = level * (RBF(length_scale = l_c,
                           length_scale_bounds=(1e-5, 1e5)) \
                       + WhiteKernel(n_level))
        Kt = Kbc(all_times.reshape([-1, 1]))
        # Generate GP sample on dense grid
        complete_GPs[:, b+1, :] = np.random.multivariate_normal(null_mean, Kt,
                                                                size = n_sig)
    # Extract for each time series
    for i in range(n_sig):
        # Random pick times without replacing
        if i == 0:
            # First sample add one more time to provide objects with irregular sampling.
            # It is not significant when generating 1000 samples.
            t_i = np.sort(np.random.choice(all_times, n_times+1, replace = False))
        else:
            # Otherwise generate q times
            t_i = np.sort(np.random.choice(all_times, n_times, replace = False))
        # Complete GP times
        complete_GPs[i, 0, :] = all_times
        # Generating times for irregular sampled GP
        GPs[i].append(t_i)

        # Linear combination of bands
        complete_GPs[i][1:] = np.dot(A_c, complete_GPs[i][1:])
        # Add means from centered GPs
        complete_GPs[i][1:] += means_c[:, all_times]
        # Construct irregular sampled GPs by random choice from line 101
        for b in range(A_c.shape[0]):
            GPs[i].append(complete_GPs[i, b+1, t_i])

        # Add associate class
        Cs.append(c)

    return np.array(GPs, dtype = object), np.array(complete_GPs, dtype = object), np.array(Cs)
